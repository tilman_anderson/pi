# Bspwm keybindings of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

# Open the default terminal emulator
alt + shift + Return
	$TERMINAL
# Open the run prompt
alt + p
	dmenu_run
# Open the application screen
alt + shift + p
    rofi -show combi
# Open alternate terminal emulators
alt + a; {t,x}
    {tabbed -c st -w,xterm}
# Toggle polybar
alt + b
    "$XDG_CONFIG_HOME"/bspwm/scripts/polybar-toggle
# Change the focused/active window border
alt + {_,shift + }c
    "$XDG_CONFIG_HOME"/bspwm/scripts/border-color -{f,a}

# Quit and restart bspwm
alt + {shift + Escape,q}
	bspc {quit,wm -r}
# Close currently focused window
alt + shift + q
	bspc node -c
# Toggle between tiled and monocle layout
alt + @space
	bspc desktop -l next

# Set the window state
alt + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# Move focus in the given direction
# alt + {_,shift + }{h,j,k,l}
# 	bspc node -{f,s} {west,south,north,east}
alt + shift + {h,j,k,l}
    bspc node -s {west,south,north,east}
# Move focus to the next or previous window
alt + {_,shift + }Tab
	bspc node -f {next,prev}.local.!hidden.window
# Focus the specified desktop or move the currently focused window there
alt + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '{1-9,10}'

# Expand the currently focused window in the given direction
alt + ctrl + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}
# Contract the currently focused window in the given direction
alt + ctrl + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}
# Move the currently focused window if floating
alt + {h,j,k,l}
	bspc node -v {-50 0,0 50,0 -50,50 0}

# Balance nodes
alt + shift + b
    bspc node @/ -B

# Preselect in the given direction
alt + {y,u,i,o}
    bspc node -p {west,south,north,east}
# Cancel the preselection of the currently focused window
alt + Escape
    bspc node -p cancel
# Mark currently focused window and send marked window to the newest preselected window
alt + {_,shift + }m
    bspc node {-g marked,newest.marked.local -n newest.!automatic.local}

# Create a receptacle in the given direction
alt + r; {h,j,k,l}
    bspc node --presel-dir {west,south,north,east} -i
# Close all receptacles
alt + shift + r
    for w in $(bspc query -N -n .leaf.!window); do bspc node $w -k; done;
