# Default config for sway
#
# Copy this to ~/.config/sway/config and edit it to your liking.
#
# Read `man 5 sway` for a complete reference.

### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod1
set $secondmod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu tofi-run | xargs swaymsg exec --

font Terminus Medium "10"

set $gap 8
gaps inner $gap

set $border 1
default_border pixel $border
default_floating_border pixel $border

smart_gaps on
hide_edge_borders smart

client.focused #ff0000 #c01c28 #ffffff #0000ff #ff0000
client.unfocused #444444 #222222 #ffffff #0000ff #444444

focus_wrapping force
mouse_warping container

seat seat0 hide_cursor when-typing enable

# Fade in
for_window [app_id=.*] opacity 0
for_window [app_id=.*] exec fadein.sh
for_window [app_id=.*] mark fade

# Fade out
bindsym Mod4+Shift+q mark quit; exec fadeout.sh

# Fadefix (uncoment if nesecary)
exec fadefix.sh

### Output configuration
#
# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output * bg ~/.local/share/wall fill
#
# Example configuration:
#
output DP-1 resolution 1680x1050@60Hz position 1920,0 transform 0
output DP-2 resolution 1920x1080@75Hz position 0,14 transform 0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

### Idle configuration
#
# Example configuration:
#
exec swayidle -w \
        timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

input "2:7:SynPS/2_Synaptics_TouchPad" {
        dwt enabled
        tap enabled
        natural_scroll disabled
        middle_emulation enabled
}

input "1739:0:Synaptics_TM3149-002" {
        dwt enabled
        tap enabled
        natural_scroll disabled
        middle_emulation enabled
}

### Key bindings
#
# Basics:
#
    # Start a terminal
    bindsym $mod+Shift+Return exec $TERMINAL

    # Kill focused window
    bindsym $mod+Shift+q kill

    # Start your launcher
    bindsym $mod+p exec $menu
    # bindsym $mod+Shift+p exec \
    #     "rofi -combi-modes drun,run -show combi -theme-str 'window{ transparency: true;}'"

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # Reload the configuration file
    bindsym $mod+q reload

    # Exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+Escape exec swaymsg exit
    bindsym $mod+Shift+Delete exec gtklock

    bindsym $mod+Shift+b exec pkill -SIGUSR1 '^waybar$'
    
    bindsym $secondmod+Tab exec ~/.config/sway/scripts/kb-layout
#
# Moving around:
#   
    bindsym $mod+Tab focus next
    bindsym $mod+Shift+Tab focus prev
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # Move the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # Ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
#
# Workspaces:
#
    # Switch to workspace
    bindsym $mod+1 workspace number 1
    bindsym $mod+2 workspace number 2
    bindsym $mod+3 workspace number 3
    bindsym $mod+4 workspace number 4
    bindsym $mod+5 workspace number 5
    bindsym $mod+6 workspace number 6
    bindsym $mod+7 workspace number 7
    bindsym $mod+8 workspace number 8
    bindsym $mod+9 workspace number 9
    bindsym $mod+0 workspace number 10
    # Move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace number 1
    bindsym $mod+Shift+2 move container to workspace number 2
    bindsym $mod+Shift+3 move container to workspace number 3
    bindsym $mod+Shift+4 move container to workspace number 4
    bindsym $mod+Shift+5 move container to workspace number 5
    bindsym $mod+Shift+6 move container to workspace number 6
    bindsym $mod+Shift+7 move container to workspace number 7
    bindsym $mod+Shift+8 move container to workspace number 8
    bindsym $mod+Shift+9 move container to workspace number 9
    bindsym $mod+Shift+0 move container to workspace number 10
    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+space layout toggle split tabbed

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+s floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+Shift+s focus mode_toggle

    # Move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

bindsym --locked XF86AudioMute exec \
    amixer -Mq -D pipewire sset Master toggle
bindsym --locked XF86AudioRaiseVolume exec \
    amixer -Mq -D pipewire sset Master 3%+
bindsym --locked XF86AudioLowerVolume exec \
    amixer -Mq -D pipewire sset Master 3%-
bindsym --locked XF86AudioMicMute exec \
    amixer -Mq -D pipewire sset Capture toggle

bindsym --locked XF86AudioPrev exec mpc prev
bindsym --locked XF86AudioPlay exec mpc toggle
bindsym --locked XF86AudioNext exec mpc next
bindsym --locked XF86AudioStop exec mpc stop

bindsym --locked $secondmod+Left exec mpc prev
bindsym --locked $secondmod+Down exec mpc toggle
bindsym --locked $secondmod+Right exec mpc next
bindsym --locked $secondmod+Up exec mpc stop

exec ~/.config/sway/startup/autostart.sh
exec_always ~/.config/sway/startup/reload.sh

include /etc/sway/config.d/*
