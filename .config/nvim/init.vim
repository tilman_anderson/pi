" Vimrc of Tilman Anderson
" Find me at https://gitlab.com/tilman_anderson.

" Tell vim to use the system clipboard
set clipboard+=unnamedplus
" Enable line numbers
set number
"set number relativenumber
" Center text on entering insert mode
autocmd InsertEnter * norm zz
" Turn on syntax highlighting
syntax on
" Regarding case sensitivity
set ignorecase
set smartcase
"Search highlighting
set hlsearch
set incsearch
" Enable the mouse
set mouse=
" Enable tab completion
set wildmode=longest,list,full
" Regarding splits
set splitbelow splitright
" Tabs appear as x spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
" Convert tabs to spaces
set expandtab
" Autoindenting
"set autoindent
" Format and encoding settings
set fileformat=unix
set encoding=utf-8
" Spelling completions
"set spell spelllang=en_us
"autocmd FileType tex,latex,markdown setlocal spell spelllang=en_us
" Line and column highlighting
"set cursorline
"set cursorcolumn
"highlight CursorLine ctermbg=Grey
"highlight CursorColumn ctermbg=Grey

let mapleader = " "
nmap <leader>mc :silent !pandoc --pdf-engine=pdflatex -f markdown -t pdf -o /tmp/vim.pdf %<cr>
nmap <leader>lc :silent !lualatex -output-directory=/tmp -jobname=vim %<cr>
nmap <leader>po :silent !zathura /tmp/vim.pdf &<cr>

hi NormalColor guifg=Black guibg=Orange ctermbg=208 ctermfg=0
hi VisualColor guifg=Black guibg=Magenta ctermbg=71 ctermfg=0
hi InsertColor guifg=Black guibg=Cyan ctermbg=75 ctermfg=0
hi ReplaceColor guifg=Black guibg=Red ctermbg=172 ctermfg=0
hi CommandColor guifg=Black guibg=Yellow ctermbg=5 ctermfg=0

hi Normalop guifg=Orange guibg=Grey ctermfg=208 ctermbg=8
hi Visualop guifg=Magenta guibg=Grey ctermfg=71 ctermbg=8
hi Insertop guifg=Cyan guibg=Grey ctermfg=75 ctermbg=8
hi Replaceop guifg=Red guibg=Grey ctermfg=172 ctermbg=8
hi Commandop guifg=Yellow guibg=Grey ctermfg=5 ctermbg=8

let g:currentmode={
	\ 'n'  : 'NORMAL',
	\ 'v'  : 'VISUAL',
	\ 'V'  : 'V·LINE',
	\ '' : 'V·BLOCK',
	\ 'i'  : 'INSERT',
	\ 'R'  : 'REPLACE',
    \ 'Rv' : 'V·REPLACE',
	\ 'c'  : 'COMMAND',
	\}

" 	\ 's'  : 'SELECT',
" 	\ 'S'  : 'S·LINE',
" 	\ '' : 'S·BLOCK',

hi File guifg=White guibg=Grey ctermbg=8 ctermfg=7
hi Fileop guifg=Grey ctermfg=8
hi Modified guifg=White ctermfg=7
hi Readonly guifg=Red ctermfg=1
hi Insertop2 guifg=Cyan ctermfg=75

set laststatus=3
set statusline=
" set statusline+=%#NormalColor#%{(mode()=='n')?'\ \ NORMAL\ ':''}
" set statusline+=%#InsertColor#%{(mode()=='i')?'\ \ INSERT\ ':''}
" set statusline+=%#ReplaceColor#%{(mode()=='R')?'\ \ REPLACE\ ':''}
" set statusline+=%#VisualColor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
set statusline+=%#NormalColor#%{(g:currentmode[mode()]=='NORMAL')?'\ \ NORMAL\ ':''}
set statusline+=%#VisualColor#%{(g:currentmode[mode()]=='VISUAL')?'\ \ VISUAL\ ':''}
set statusline+=%#VisualColor#%{(g:currentmode[mode()]=='V·LINE')?'\ \ V·LINE\ ':''}
set statusline+=%#VisualColor#%{(g:currentmode[mode()]=='V·BLOCK')?'\ \ V·BLOCK\ ':''}
set statusline+=%#InsertColor#%{(g:currentmode[mode()]=='INSERT')?'\ \ INSERT\ ':''}
set statusline+=%#ReplaceColor#%{(g:currentmode[mode()]=='REPLACE')?'\ \ REPLACE\ ':''}
set statusline+=%#ReplaceColor#%{(g:currentmode[mode()]=='V·REPLACE')?'\ \ V·REPLACE\ ':''}
set statusline+=%#CommandColor#%{(g:currentmode[mode()]=='COMMAND')?'\ \ COMMAND\ ':''}
set statusline+=%#Normalop#%{(g:currentmode[mode()]=='NORMAL')?'':''}
set statusline+=%#Visualop#%{(g:currentmode[mode()]=='VISUAL')?'':''}
set statusline+=%#Visualop#%{(g:currentmode[mode()]=='V·LINE')?'':''}
set statusline+=%#Visualop#%{(g:currentmode[mode()]=='V·BLOCK')?'':''}
set statusline+=%#Insertop#%{(g:currentmode[mode()]=='INSERT')?'':''}
set statusline+=%#Replaceop#%{(g:currentmode[mode()]=='REPLACE')?'':''}
set statusline+=%#Replaceop#%{(g:currentmode[mode()]=='V·REPLACE')?'':''}
set statusline+=%#Commandop#%{(g:currentmode[mode()]=='COMMAND')?'':''}
" set statusline+=%#CursorColumn#
" set statusline+=\ %{g:currentmode[mode()]}\ 
set statusline+=%#File#
set statusline+=\ %F\ 
set statusline+=%#Fileop#
set statusline+=%#Modified#
set statusline+=%{&modified?'\ +\ ':''}
set statusline+=%#Readonly#
set statusline+=%{&readonly?'\ RO\ ':''}
set statusline+=%=
set statusline+=%#Insertop2#
set statusline+=%#InsertColor#
set statusline+=\ %p%%\ 
set statusline+=%l:%c\ 

set noshowmode

ino " ""<left>
ino ' ''<left>
ino ( ()<left>
ino [ []<left>
ino { {}<left>
" ino {<CR> {<CR>}<ESC>O
