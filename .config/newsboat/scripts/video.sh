#!/bin/sh

mpv --fullscreen \
    --msg-level='all'='no' \
    --script-opts='ytdl_hook-ytdl_path'='yt-dlp' \
    --ytdl-format='bestvideo[height<=?1080][fps<=?60]+bestaudio/best' \
    "$@"
exit 0
