#!/bin/sh

/usr/lib/polkit-kde-authentication-agent-1  &   # Authentication
# lxpolkit &                                      # Authentication

# pipewire &                          # Pipewire Media Server
mpd &                               # Music Player Daemon
dunst &                             # Notification

fwall ~/.local/share/wall &         # Wallpaper
# swaybg -i ~/.local/share/wall &     # Wallpaper
swayidle -w \
    lock '~/.config/hypr/scripts/lock' &
    # timeout 300 'loginctl lock-session' \
    # timeout 420 'hyprctl dispatch dpms off' \
    #     resume 'hyprctl dispatch dpms on' \
    # before-sleep 'loginctl lock-session' &

gammastep -m wayland -PO 4500K &

[ -f "$XDG_CACHE_HOME"/touchpad-disabled ] && hyprctl keyword device:synaptics-tm3149-002:enabled false

hyprctl setcursor Neutral 24

gsettings set org.gnome.desktop.interface gtk-theme     'Artix-dark'
gsettings set org.gnome.desktop.interface icon-theme    'Adwaita'
gsettings set org.gnome.desktop.interface cursor-theme  'Neutral'
gsettings set org.gnome.desktop.interface font-name     'Terminus'

# [ -f ~/.local/share/sound ] && mpv ~/.local/share/sound
