#!/bin/sh

if [ "$1" = "-f" ]; then
    kind="active"
elif [ "$1" = "-i" ]; then
    kind="inactive"
else
    printf "%sThis script supports the following flags:\n-f --- Change the color of the focused window border in hyprland\n-i --- Change the color of the inactive window border in hyprland\nOnly the first flag is read.\n" && \
    exit 1
fi

red="ff0000"
green="00ff00"
yellow="ffff00"
blue="0000ff"
magenta="ff00ff"
cyan="00ffff"

current_hex="$(awk '/col.'"$kind"'_border =/ {print $NF}' "$XDG_CONFIG_HOME"/hypr/hyprland.conf | sed 's/rgb(//g' | sed 's/)//g')"
list="$(printf "%s$red\n$green\n$yellow\n$blue\n$magenta\n$cyan" | nl)"

for c in $(printf '%s\n' "$list" | awk '{print $NF}'); do
    [ "$c" = "$current_hex" ] && break
    [ "$c" = "$cyan" ] && \
    printf "%sChange your $kind border color in hyprland to one of the supported colors:\n- #$red\n- #$green\n- #$yellow\n- #$blue\n- #$magenta\n- #$cyan\n" && \
    exit 1
done

current_digit="$(printf '%s\n' "$list" | awk '/'"$current_hex"'/ {print $1}')"
next_digit="$(printf '%s\n' "$current_digit+1" | bc)"
[ "$next_digit" = "7" ] && next_digit="1"
next_hex="$(printf '%s\n' "$list" | awk '/'"$next_digit"'/ {print $NF}')"

hyprctl keyword general:col."$kind"_border "rgb($next_hex)"
sed -i 's|col.'"$kind"'_border = rgb(.*)|col.'"$kind"'_border = rgb('"$next_hex"')|' "$XDG_CONFIG_HOME"/hypr/hyprland.conf
