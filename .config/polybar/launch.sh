#!/bin/sh
polybar-msg cmd quit
for m in $(polybar --list-monitors | awk -F : '{print $1}'); do
    MONITOR=$m polybar -r bar &
done
