#!/bin/sh

sed 's/"monitor"\:[[:blank:]]*0,$/"monitor"\:1,/g' "$XDG_CONFIG_HOME"/HybridBar/config.json \
    | sed '/"right-cava_main":/,+1 d' \
    > "$XDG_CONFIG_HOME"/HybridBar/second.json

killall -q hybrid-bar
sleep 1
pgrep hybrid-bar || HYBRID_CONFIG=config.json hybrid-bar &
[ "$(pgrep hybrid-bar | wc -l)" -gt 1 ] || HYBRID_CONFIG=second.json hybrid-bar &
