# Dunstrc of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

[global]
    monitor = 0
    follow = keyboard
    width = 300
    height = 750
    origin = top-right
    offset = 15x15

    indicate_hidden = yes

    transparency = 0

    padding = 5
    horizontal_padding = 5
    frame_width = 1
    gap_size = 8
    sort = yes

    idle_threshold = 120

    font = Terminus 11
    line_height = 2
    markup = full
    #   %a  appname
    #   %s  summary
    #   %b  body
    #   %i  iconname (including its path)
    #   %I  iconname (without its path)
    #   %p  progress value if set ([  0%] to [100%]) or nothing
    #   %n  progress value if set without any extra characters
    #   %%  Literal %
    format = "<b>%s</b>\n%b"

    # Possible values are "left", "center" and "right".
    alignment = center
    # Possible values are "top", "center" and "bottom".
    vertical_alignment = center
    # Set to -1 to disable.
    show_age_threshold = 60
    # Possible values are "start", "middle" and "end".
    ellipsize = middle
    ignore_newline = no
    stack_duplicates = true
    hide_duplicate_count = false
    show_indicators = yes

    # Align icons left/right/top/off
    icon_position = off

    sticky_history = yes
    history_length = 20

    dmenu = /usr/local/bin/dmenu -p dunst:
    browser = /usr/bin/xdg-open

    always_run_script = true
    title = Dunst
    class = Dunst

    corner_radius = 0
    ignore_dbusclose = false

    force_xwayland = false
    force_xinerama = false

    mouse_left_click = close
    mouse_middle_click = none
    mouse_right_click = close-all

[experimental]
    per_monitor_dpi = false

[urgency_low]
    background = "#12488b"
    foreground = "#c5c8c6"
    frame_color = "#0080ff"
    timeout = 6
[urgency_normal]
    background = "#180055"
    foreground = "#c5c8c6"
    frame_color = "#0000ff"
    timeout = 6
[urgency_critical]
    background = "#c01c28"
    foreground = "#c5c8c6"
    frame-color = "#ff0000"
    timeout = 0
# vim: ft=cfg
