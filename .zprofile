# Zprofile of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

export BROWSER="qutebrowser"
export EDITOR="vim"
export TERMINAL="st"

export PATH="$HOME/.local/bin:$PATH"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"

export QT_QPA_PLATFORMTHEME="qt5ct"

export LESSHISTFILE="-"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx "$HOME"/.config/X11/xinitrc
fi
