# Bash profile of Tilman Anderson
# Find me at https://gitlab.com/tilman_anderson.

[[ -f ~/.bashrc ]] && . ~/.bashrc

export BROWSER="firefox"
export EDITOR="nvim"
export TERM="foot"
export TERMINAL="foot"
# export RUN="bemenu-run"
export RUN="tofi-run"
export LAUNCHER="tofi"

export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"

export QT_QPA_PLATFORMTHEME="qt6ct"

export LESSHISTFILE="-"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export MPLAYER_HOME="$XDG_CONFIG_HOME/mplayer"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# export LD_LIBRARY_PATH="/usr/local/lib"
# export BEMENU_RENDERERS="/usr/local/lib/bemenu"
export BEMENU_OPTS="--ignorecase \
    --prompt '' \
    --list 25 \
    --wrap \
    --center \
    --border 1 \
    --bdr #005577 \
    --ch 14 \
    --cw 1 \
    --no-overlap \
    --width-factor 0.5 \
    --fn Terminus 14 \
    --nf #bbbbbb \
    --nb #222222 \
    --hf #eeeeee \
    --hb #005577 \
    --tf #eeeeee \
    --tb #005577 \
    --af #bbbbbb \
    --ab #111111 \
    --ff #bbbbbb \
    --fb #111111"

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
    exec ~/.config/hypr/startup/launch.sh
fi
